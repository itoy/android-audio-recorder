# Translations

  * Chinese translation thanks to @itoy
  * Chinese (Taiwan) translation thanks to @cges30901
  * Brazilian translation thanks to @vrozsas
  * Danish translation thanks to @lianergoist
  * Dutch translation thanks to @Stephan-P
  * French translation thanks to @Nonot
  * German translation thanks to @s72785
  * Greek translation thanks to @cryoranger
  * Indonesian translation thanks to @ditokp, @zmni
  * Italian translation thanks to @Agno94
  * Japanese translation thanks to @naofumi
  * Polish translation thanks to @krkk
  * Slovak translation thanks to @pvagner
  * Spanish translation thanks to @sguinetti
  * Turkish translation thanks to @tneonflo
